//
//  ViewController.swift
//  ChatClient
//
//  Created by Kazuya Shida on 2018/11/12.
//  Copyright © 2018 Unifa. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.performSegue(withIdentifier: "NavigationControllerSegue", sender: nil)
    }
}
