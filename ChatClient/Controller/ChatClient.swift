//
//  ChatClient.swift
//  ChatClient
//
//  Created by Kazuya Shida on 2018/11/16.
//  Copyright © 2018 Unifa. All rights reserved.
//

import Network

import RxSwift
import RxCocoa

struct Message {
    let text: String
    let isReceived: Bool
}

class ChatClient {
    let connection: NWConnection
    let queue = DispatchQueue(label: "com.unifa-e.ChatClient")

    /// 送受信したメッセージ
    var messages = BehaviorRelay<[Message]>(value: [])

    init(ip: String, port: UInt16) {
        let host = NWEndpoint.Host(ip)
        let port = NWEndpoint.Port(integerLiteral: port)

        /// NWConnection にホスト名とポート番号を指定して初期化
        self.connection = NWConnection(host: host, port: port, using: .tcp)
        
        /// コネクションのステータスを監視のイベントハンドラ
        self.connection.stateUpdateHandler = { (newState) in
            NSLog("\(#function), \(newState)")
            switch newState {
            case .ready:
                NSLog("Ready to send")
            case .waiting(let error):
                NSLog("\(#function), \(error)")
            case .failed(let error):
                NSLog("\(#function), \(error)")
            case .setup: break
            case .cancelled: break
            case .preparing: break
            }
        }
        
        /// コネクションの開始
        self.connection.start(queue: queue)
        self.receive(on: connection)
    }

    deinit {
        connection.cancel()
    }

    func receive(on connection: NWConnection) {
        /// コネクションからデータを受信
        connection.receive(minimumIncompleteLength: 0, maximumLength: Int(UInt32.max)) { [weak self] (data, _, _, error) in
            if let error = error {
                NSLog("\(#function), \(error)")
                connection.cancel()
                return
            }
            if let data = data {
                #if DEBUG
                let bytes: String = data[0..<min(data.count, 150)].map { String(format: "%02X ", $0) }.joined()
                NSLog("[Received]: \(bytes)")
                #endif
                let text = String(data: data, encoding: .utf8)!
                let message = Message(text: text, isReceived: true)
                self?.messages.acceptAppending(message)
                self?.receive(on: connection)
            } else {
                NSLog("\(#function), Received data is nil")
            }
        }
    }

    func send(text: String) {
        let message = "\(text)\n"
        let data = message.data(using: .utf8)!

        /// メッセージの送信
        connection.send(content: data, completion: .contentProcessed { [unowned self] (error) in
            if let error = error {
                NSLog("\(#function), \(error)")
            } else {
                #if DEBUG
                let req = data.map { String(format: "%02X ", $0) }.joined()
                NSLog("[Sent]: \(req)")
                #endif
                let message = Message(text: text, isReceived: false)
                self.messages.acceptAppending(message)
            }
        })
    }
}

extension BehaviorRelay where Element: RangeReplaceableCollection {
    func acceptAppending(_ element: Element.Element) {
        accept(value + [element])
    }
}
