//
//  ChatViewController.swift
//  ChatClient
//
//  Created by Kazuya Shida on 2018/11/12.
//  Copyright © 2018 Unifa. All rights reserved.
//

import UIKit

import RxSwift
import RxCocoa

class ChatViewController: UIViewController {
    let dispose = DisposeBag()

    @IBOutlet var tableView: UITableView!
    @IBOutlet var textField: UITextField!

    var ip: String = ""
    var port: UInt16 = 0
    lazy var chatClient = ChatClient(ip: ip, port: port)

    override func viewDidLoad() {
        super.viewDidLoad()

        chatClient.messages.asObservable()
            .bind(to: tableView.rx.items(cellIdentifier: "ChatCell")) { (_, item: Message, cell) in
                cell.textLabel?.text = item.text
                cell.textLabel?.textAlignment = item.isReceived ? .right : .left
            }
            .disposed(by: dispose)
    }

    @IBAction func sendMessage(_ sender: UIButton) {
        if let message = textField.text {
            chatClient.send(text: message)
            textField.text = ""
            view.endEditing(true)
        }
    }
}
