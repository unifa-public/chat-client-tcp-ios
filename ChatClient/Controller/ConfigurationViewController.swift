//
//  ConfigurationViewController.swift
//  ChatClient
//
//  Created by Kazuya Shida on 2018/11/12.
//  Copyright © 2018 Unifa. All rights reserved.
//

import UIKit

import RxSwift
import RxCocoa
import RxDataSources

class ConfigurationViewController: UIViewController {
    let dispose = DisposeBag()
    var configuration = Configuration()

    @IBOutlet var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        let connectItem = UIBarButtonItem(
            title: "Connect", style: .plain, target: self, action: #selector(connect(_:)))
        navigationItem.rightBarButtonItem = connectItem

        tableView.rx.setDelegate(self)
            .disposed(by: dispose)

        let dataSource = RxTableViewSectionedReloadDataSource<ConfigurationSection>(configureCell: { _, tableView, index, item in
            let configurationCell = tableView.dequeueReusableCell(withIdentifier: item.cell, for: index)
            guard let cell = configurationCell as? ConfigurationCell else {
                return configurationCell
            }
            cell.ipField?.text = item.config.ip.value
            cell.ipField?.rx.text
                .throttle(0.3, scheduler: MainScheduler.instance)
                .map { (text) -> String in text ?? "" }
                .bind(to: item.config.ip)
                .disposed(by: cell.dispose)
            cell.portField?.text = item.config.port.value
            cell.portField?.rx.text
                .throttle(0.3, scheduler: MainScheduler.instance)
                .map { (text) -> String in text ?? "" }
                .bind(to: item.config.port)
                .disposed(by: cell.dispose)
            return cell
        })

        let sections: [ConfigurationSection] = [
            ConfigurationSection(header: "Config", items: [
                ConfigurationItem(title: "ip", cell: "IpCell", config: configuration),
                ConfigurationItem(title: "port", cell: "PortCell", config: configuration)
                ])
        ]
        Observable.from(optional: sections)
            .bind(to: tableView.rx.items(dataSource: dataSource))
            .disposed(by: dispose)

        configuration.asObservable
            .map { $0.isValid }
            .bind(to: connectItem.rx.isEnabled)
            .disposed(by: dispose)
    }

    @objc func connect(_ sender: UIBarButtonItem) {
        self.performSegue(withIdentifier: "ChatSegue", sender: configuration)
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewController = segue.destination as? ChatViewController,
            let confg = sender as? Configuration {
            viewController.ip = confg.ip.value
            viewController.port = UInt16(confg.port.value) ?? 9999
        }
    }
}

// MARK: - UIScrollViewDelegate

extension ConfigurationViewController: UIScrollViewDelegate {

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
}

// MARK: - UITableViewCell

class ConfigurationCell: UITableViewCell {
    var dispose = DisposeBag()

    @IBOutlet var ipField: UITextField?
    @IBOutlet var portField: UITextField?

    override func prepareForReuse() {
        super.prepareForReuse()
        dispose = DisposeBag()
    }
}

// MARK: - Configuration

struct Configuration {
    var ip = BehaviorRelay<String>(value: "localhost")
    var port = BehaviorRelay<String>(value: "9999")

    var asObservable: Observable<Configuration> {
        let ip = self.ip.asObservable()
        let port = self.port.asObservable()
        return Observable.combineLatest(ip, port) { (_, _) in self }
    }

    var isValid: Bool {
        return !ip.value.isEmpty && !port.value.isEmpty
    }
}

// MARK: - ConfigurationItem

struct ConfigurationItem {
    var title: String
    var cell: String
    var config: Configuration
}

extension ConfigurationItem: IdentifiableType, Equatable {
    typealias Identity = String

    var identity: String {
        return title
    }
}

func == (rhs: ConfigurationItem, lhs: ConfigurationItem) -> Bool {
    return rhs.title == lhs.title
}

// MARK: - ConfigurationSection

struct ConfigurationSection {
    var header: String
    var items: [Item]
}

extension ConfigurationSection: AnimatableSectionModelType {
    typealias Item = ConfigurationItem
    var identity: String {
        return header
    }

    init(original: ConfigurationSection, items: [ConfigurationItem]) {
        self = original
        self.items = items
    }
}
